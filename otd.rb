require 'sinatra'
require 'httparty'

get '/' do
  user  = params['user']
  goal  = params['goal']
  token = params['token']
  base  = params['base'] || '1500'
  base  = nil unless base =~ /\d\d\d\d/

  return "Usage: ?user=USER&goal=GOAL&token=TOKEN[&base=BASE]" unless user && goal && token && base

  base    = base[0..1] + ':' + base[2..3]
  target  = Time.parse Time.now.strftime("%Y-%m-%dT#{base}")
  seconds = (Time.now - target).to_i % 86400
  value   = seconds / 60

  url = "https://www.beeminder.com/api/v1/users/#{user}/goals/#{goal}/datapoints.json?auth_token=#{token}"
  options = {:query => {:timestamp => Time.now.to_i, :value => value}}
  HTTParty.post(url, options).to_s
end
