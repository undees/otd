require File.dirname(__FILE__) + '/../../otd.rb'

require 'rack/test'
require 'rspec/expectations'
require 'webmock/cucumber'
require 'timecop'

class OtdWorld
  include Rack::Test::Methods

  def app
    Sinatra::Application
  end
end

World { OtdWorld.new }
