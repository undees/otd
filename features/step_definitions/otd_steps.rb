require 'time'
require 'json'

Given(/^I want to leave at (\d+:\d+)$/) do |time|
  @desired = Time.parse(time)
end

When(/^I leave at (\d+:\d+)$/) do |time|
  achieved  = Time.parse(time)
  mins_late = Integer((achieved - @desired) / 60.0)
  goal = "GOAL"
  base = @desired.strftime("%02H%02M")

  timestamp = achieved.to_i

  url = "https://www.beeminder.com/api/v1/users/USER/goals/GOAL/datapoints.json?auth_token=TOKEN&timestamp=#{timestamp}&value=#{mins_late}"

  stub_request(:post, url).
    to_return(:body => %Q|{"value":#{mins_late}}|)

  Timecop.freeze(achieved) do
    get "/?user=USER&goal=GOAL&token=TOKEN&base=#{base}"
  end
end

Then(/^I am (\d+) minutes late$/) do |minutes|
  response = JSON.parse last_response.body
  late = response["value"]
  late.should be_within(0.001).of(minutes.to_f)
end
